def change(cash):
	if cash == 1 or cash == 3:
		return None
	basic_dict = dict(((2,'2'),(4,'2,2'),(5,'5'),(6,'2,2,2'),(7,'5,2'),(8,'2,2,2,2'),(9,'5,2,2'),(10,'10')))
	str_cash = int(str(cash)[-1])
	if str_cash == 1 or str_cash == 3:
		div10 = cash // 10
		if div10 > 1:
			ten = div10-1
		else:
			ten = 0
		if str_cash == 1:
			eleven = basic_dict[2]+basic_dict[9]
			two = eleven.count('2')
			five = eleven.count('5')
		else:
			thirteen = basic_dict[4]+basic_dict[9]
			two = thirteen.count('2')
			five = thirteen.count('5')
	else:
		if cash <= 10:
			two = basic_dict[cash].count('2')
			five = basic_dict[cash].count('5')
			ten = basic_dict[cash].count('10')
		else:
			mod10 = cash%10
			ten = cash // 10
			if mod10 == 0:
				two = 0
				five = 0
			else:
				n_cash = cash - (10*ten)
				two = basic_dict[n_cash].count('2')
				five = basic_dict[n_cash].count('5')
	return {
		'two': two,
		'five': five,
		'ten': ten
	}


a = change(11)
print(a)
a = change(120)
print(a)
a = change(13)
print(a)
a = change(8)
print(a)